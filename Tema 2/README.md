# Aydogan_Mert_EGC_Lab3

# EGC Laborator 2 - Referat

1. Creați un proiect elementar. Urmăriți exemplul furnizat cu titlu de demonstrație
OpenTK_console_sample01 și OpenTK_console_sample02. Atenție la setarea viewport-ului.
Modificați valoarea constantei „MatrixMode.Projection”. Ce observați?
R: Nu cred ca se poate modifica.

3. Răspundeți la următoarele întrebări (utilizați ca referință și tutorialele OpenGL Nate Robbins
încărcate în cadrul laboratorului 01):

1. Ce este un viewport?
Un viewport reprezinta o regiune de vizualizare a poligonului in grafica computerizata. În OpenGL este modalitate de afisare a datelor si coordonatelor dupa nevoie.
2. Ce reprezintă conceptul de frames per seconds din punctul de vedere al bibliotecii OpenGL?
R: Coneptulde frames per seconds (fps) reprezinta de cate ori este randata imaginea pe ecran.
3. Cândeste rulată metoda OnUpdateFrame()?
R: Metoda OnUpdateFrame() este rulata de fiecare data cand se schimba frame-ul.
4. Ce este modul imediat de randare?
R: Modul imediat de randare reprezinta folosirea GL.BEGIN(), GL.VERTEX si GL.END() pentru desenarea poligoanelor. Un dezavantaj ar fi timpul mare de desenare.
5. Care este ultima versiune de OpenGL care acceptă modul imediat?
R: Ultima versiune de OpenGL care accepta modul imediat este 3.2.
6. Cândeste rulată metoda OnRenderFrame()?
R: Metoda este redata atunci cand se incepe randarea scenei.
7. De ce este nevoie ca metoda OnResize() să fie executată cel puțin o dată?
R: Este nevoie de executarea metodei OnResize() cel putin o data pentru setarea sau modificarea Viewport-ului.
8. Ce reprezintă parametrii metodei CreatePerspectiveFieldOfView() și care este domeniul de valori pentru aceștia?
    1. Parametrul fovy: unghiul de vizualzare in planul xoy, domeniul este [0.0, 108.0].
    2. Parametrul aspect: raportul dintre lungimea si inaltimea ferestrei in planul apropiat.
    3. Parametrul zNear: distanta minima de la care se pot observa obiecte.
    4. Parametrul zFar: distanta maxima de la care se pot observa obiecte.

